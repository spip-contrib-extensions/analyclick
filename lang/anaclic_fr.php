<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(


	'statistiques_documents'		=> 'T&eacute;l&eacute;chargements',
	'configurer'					=> 'Configuration des t&eacute;l&eacute;chargements',
	'configurer_info'				=> 'Choississez un d&eacute;lais entre deux t&eacute;l&eacute;chargements. Les clics pendant ce d&eacute;lais ne seront pas comptabilis&eacute;s.<br>Choississez un d&eacute;lais n&eacute;gatif pour un d&eacute;lais infini (un seul t&eacute;l&eacute;chargement par IP).',
	'delais'						=> 'D&eacute;lais entre deux t&eacute;l&eacute;chargements',
	'info_afficher'					=> 'Afficher les statistiques de :',
	'info_telechargement_30'		=> 'Afficher les statistiques de t&eacute;l&eacute;chargement <b>sur les 30 derniers jours</b> :',
	'info_telechargement'			=> 'Afficher les statistiques de t&eacute;l&eacute;chargement <b>depuis le d&eacute;but</b> :',
	'no_statistique'				=> 'Pas de statistique disponible'

);


